package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import ViewModels.AudioViewModel;
import Models.Audio;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de los audios
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class AudiosPage extends ViewBase
{
    /**
     * Contructor
     */
    public AudiosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new AudioViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Audios";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
}
