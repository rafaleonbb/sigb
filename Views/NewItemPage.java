package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import Models.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Util.SigbUtilities;


/**
 * Nuevo item
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public abstract class NewItemPage extends ViewBase
{
    /**
     * Contructor
     */
    NewItemPage(Application app)
    {
        super(app);
    }
    
    /**
     * Panel para los entries
     */
    protected JPanel panel;
    /**
     * Campos para la toma de datos
     */
    protected JTextField nameText;
    protected JTextField isbnText;
    protected JTextField editorialText;
    protected JComboBox  genderPicker;
    
    /**
     * Para controlar el número de elemento en el SpringLayout
     */
    protected int numPairs;
  
    /**
     * Método a implementar en las sub-clases para preparar el item para almacenarlo
     */
    abstract Item prepareItem();
    
    /**
     * Implementar en las sub-clases el tamaño de la pantalla
     */
    abstract void setSizePage();
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();
        configureFieldsData();
    }
    
    /**
     * Configurar la ventana
     */
    @Override
    protected void configure()
    {
        setTitle(getTitlePage());
        setSizePage();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                dispose();        
            }
        });
    }
    
    /**
     * Configura los entries comunes de los items analogicos
     */
    protected void configureFieldsData()
    {
        numPairs = 0;
        JLabel label;
        
        panel = new JPanel(new SpringLayout());
        getContentPane().add(panel);
        
        label = new JLabel("ISBN/Código: ", JLabel.TRAILING);
        isbnText = new JTextField(10);
        label.setLabelFor(isbnText);
        panel.add(label);
        panel.add(isbnText);
        ++numPairs;
        
        label = new JLabel("Título: ", JLabel.TRAILING);
        nameText = new JTextField(10);
        label.setLabelFor(nameText);
        panel.add(label);
        panel.add(nameText);
        ++numPairs;
        
        label = new JLabel("Editorial/Distribuidora: ", JLabel.TRAILING);
        editorialText = new JTextField(10);
        label.setLabelFor(editorialText);
        panel.add(label);
        panel.add(editorialText);
        ++numPairs;
        
        label = new JLabel("Género: ", JLabel.TRAILING);
        genderPicker = new JComboBox(((BaseViewModel)viewModel).getGenderList());        
        panel.add(label);
        panel.add(genderPicker);
        genderPicker.setSelectedIndex(0);
        ++numPairs;        
        
    }    
    
    /**
     * Poner los botones de aceptar y cancelar
     */
    protected void putButtons()
    {
        // Botón para almacenar
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {     
                if (validateFields()) {
                    
                    // Pasar el item al viewmodel para que lo inserte
                    viewModel.insert(prepareItem());
                    
                    // Limpiar los campos
                    clearEntries();
                }
            }
        });               
        
        // Botón para anular
        JButton cancelButton = new JButton("Cancelar");
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {     
                // Limpiar los campos
                clearEntries();
            }
        });               
        panel.add(okButton);
        panel.add(cancelButton);
        ++numPairs;        
    }
  
    /**
     * Valida la toma de datos
     */
    protected boolean validateFields()
    {
        boolean result = true;
        
        if (nameText.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe introducir el nombre");
            result = false;
        } else if (editorialText.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe introducir la editorial o distribuidora");
            result = false;
        } 
        return result;
    }    
    
    /**
     * Borrar las cajas de texto
     */
    protected void clearEntries()
    {
        nameText.setText("");
        editorialText.setText("");
        genderPicker.setSelectedItem(0);
    }
}
