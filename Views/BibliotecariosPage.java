package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import ViewModels.BibliotecarioViewModel;
import Models.Bibliotecario;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de los bibliotecario
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class BibliotecariosPage extends ViewBase
{
    /**
     * Contructor
     */
    public BibliotecariosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new BibliotecarioViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Bibliotecarios";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
    /**
     * Configurar el meú
     */
    protected void configureMenu()
    {
        JMenuBar barra = new JMenuBar();
        setJMenuBar(barra);
        JMenu menu = new JMenu("Mantenimiento");
        barra.add(menu);
        
        JMenuItem item;
        item = new JMenuItem("Eliminar");
        item.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                OnDeleteClick((Bibliotecario)list.getSelectedValue());
            }
        });
        menu.add(item);        
    }
}
