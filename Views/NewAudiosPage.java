package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.AudioViewModel;
import Models.Audio;
import Models.Gender;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Util.SigbUtilities;


/**
 * Nuevo audio
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class NewAudiosPage extends NewItemDigitalPage
{
    /**
     * Contructor
     */
    public NewAudiosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new AudioViewModel(App);
    }
    
    /**
     * Título de la página
     */
    String getTitlePage()
    {
        return "Nuevo audio";
    }    
    
    /**
     * Preparar el item para almacenarlo
     */
    protected Audio prepareItem()
    {
        // Generar un nuevo item y darle los valores de los Entries
        Audio item = (Audio)viewModel.getType();
        item.setIsbn(isbnText.getText());
        item.setTitle(nameText.getText());
        item.setEditorial(editorialText.getText());
        item.setGenero((Gender)genderPicker.getSelectedItem());
        item.setDuration(Integer.parseInt(durationText.getText()));
        item.setSize(Integer.parseInt(sizeText.getText()));
        return item;
    }
    
}
