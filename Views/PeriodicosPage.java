package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import ViewModels.PeriodicoViewModel;
import Models.Periodico;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de los periódicos
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class PeriodicosPage extends ViewBase
{
    /**
     * Contructor
     */
    public PeriodicosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new PeriodicoViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Periodicos";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
}
