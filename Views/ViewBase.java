package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import Models.IAlmacenable;
import Models.Item;
import Models.Prestamo;
import Views.Application;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Base para las vistas
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public abstract class ViewBase extends JFrame
{
    /**
     * Acceso a la aplicación principal. De esta forma se simula una field readonly
     */
    public final Application App;
    
    /**
     * Lista con los elementos de la vista
     */
    protected JList list;
    
    /**
     * Título de la ventana
     */
    protected String title;
    
    /**
     * Campo que contendrá la instancia del ViewModel concreto
     */
    protected BaseViewModel viewModel;
    
    /**
     * Método que implementarán las subclases para crear los componentes de la pantalla
     */
    abstract void initializeComponents();
    
    
    /**
     * Método que implementarán las subclases para instanciar el viewmodel concreto
     */
    abstract void createViewModel();    
    
    /**
     * Título de la ventana (implementar en las subclases)
     */
    abstract String getTitlePage();    
    
    
    /**
     * Constructor
     */
    ViewBase(Application app)
    {
        super();
        
        App = app;
        
        createViewModel();
    
        initializeComponents();
    }
    
    /**
     * Configurar la ventana
     */
    protected void configure()
    {
        setTitle(getTitlePage());
        setSize(640, 480);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                OnClose(e);
            }
        });
    }   
    
    
    /**
     * Configurar la lista
     */
    protected void configureList()
    {
        list = new JList(viewModel.getVisibleData());
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        //list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        getContentPane().add(panel);
        
        panel.add(list, BorderLayout.CENTER);        
    }       

    
    
    /**
     * Configurar el menú
     */
    protected void configureMenu()
    {
        JMenuBar barra = new JMenuBar();
        setJMenuBar(barra);
        if (App.isAdmin()) {
            JMenu menu = new JMenu("Mantenimiento");
            barra.add(menu);
            
            JMenuItem item;
            item = new JMenuItem("Eliminar");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    OnDeleteClick((IAlmacenable)list.getSelectedValue());
                }
            });
            menu.add(item);   
        }
        
        if (viewModel.getType() instanceof Item) {
            JMenu menu = new JMenu("Préstamos");
            barra.add(menu);
            
            JMenuItem item;
            item = new JMenuItem("Comprobar disponiblidad");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    if (list.getSelectedValue() != null) {
                        viewModel.checkDisponibilidad((Item)list.getSelectedValue());
                    }
                }
            });            
            menu.add(item);        
            
            item = new JMenuItem("Solicitar préstamo");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    Item elemento = (Item)list.getSelectedValue();
                    if (list.getSelectedValue() != null) {
                        viewModel.solicitarPrestamo(App.getUsuario(), elemento);
                    }
                }
            });
            menu.add(item);        
            
            item = new JMenuItem("Reservar");
            item.addActionListener(new ActionListener () {
                public void actionPerformed(ActionEvent e) {
                    Item elemento = (Item)list.getSelectedValue();
                    if (list.getSelectedValue() != null) {
                        viewModel.reservar(App.getUsuario(), elemento);
                    }
                }
            });
            menu.add(item);        
        }
        
    }

    /**
     * Evento al cerrar la pantalla. Automatiza el almacenar los datos
     */
    protected void OnClose (java.awt.event.WindowEvent e)
    {
         viewModel.saveData();
         this.dispose();        
    }    

    /**
     * Evento al pulsar la opción de eliminar
     */
    protected void OnDeleteClick(IAlmacenable item) 
    {
        if (item != null) {
            viewModel.delete(item);
        }
    }
}
