package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.BaseViewModel;
import ViewModels.UsuarioViewModel;
import Models.Usuario;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 * Vista de los usuarios
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class UsuariosPage extends ViewBase
{
    /**
     * Contructor
     */
    public UsuariosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new UsuarioViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Usuarios";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
    /**
     * Configurar el meú
     */
    protected void configureMenu()
    {
        JMenuBar barra = new JMenuBar();
        setJMenuBar(barra);
        JMenu menu = new JMenu("Mantenimiento");
        barra.add(menu);
        
        JMenuItem item;
        
        item = new JMenuItem("Imprimir tarjeta");
        item.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                UsuarioViewModel vm = (UsuarioViewModel)viewModel;
                vm.print((Usuario)list.getSelectedValue());
            }
        });
        menu.add(item);        
        
        item = new JMenuItem("Eliminar");
        item.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                OnDeleteClick((Usuario)list.getSelectedValue());  
            }
        });
        menu.add(item);        
    }
}
