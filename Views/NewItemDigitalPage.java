package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.AudioViewModel;
import Models.Audio;
import Models.Gender;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Util.SigbUtilities;


/**
 * Nuevo audio
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public abstract class NewItemDigitalPage extends NewItemPage
{
    /**
     * Contructor
     */
    NewItemDigitalPage(Application app)
    {
        super(app);
    }
    
    /**
     * Entries específicos de un item digital
     */
    protected JTextField durationText;
    protected JTextField sizeText;    
    
    /**
     * Tamaño de la ventana
     */
    void setSizePage()
    {
        setSize(500, 220);
    }
    
    /**
     * Preparar la toma de datos
     */
    @Override
    protected void configureFieldsData()
    {
        super.configureFieldsData();        
        
        JLabel label;
        
        label = new JLabel("Duración (mins.): ", JLabel.TRAILING);
        durationText = new JTextField(10);
        label.setLabelFor(durationText);
        panel.add(label);
        panel.add(durationText);
        ++numPairs;
        
        label = new JLabel("Tamaño (Mb.): ", JLabel.TRAILING);
        sizeText = new JTextField(10);
        label.setLabelFor(sizeText);
        panel.add(label);
        panel.add(sizeText);
        ++numPairs;
        
        putButtons();
        
        SpringUtilities.makeCompactGrid(panel,
            numPairs, 2, //rows, cols
            6, 6,        //initX, initY
            6, 6);       //xPad, yPad        
    }    

    /**
     * Validaciones específicas del item
     */
    @Override
    protected boolean validateFields()
    {
        // Primero valido los campos comunes
        boolean result = super.validateFields();
        
        if (result) {
            if (!SigbUtilities.tryParseInt(durationText.getText())) {
                JOptionPane.showMessageDialog(null, "Debe introducir la duración");
                result = false;
            } else if (!SigbUtilities.tryParseInt(sizeText.getText())) {
                JOptionPane.showMessageDialog(null, "Debe introducir el tamaño del archivo");
                result = false;
            }
        }
        return result;
    }
    
    /**
     * Borrar las cajas de texto
     */
    @Override
    protected void clearEntries()
    {
        super.clearEntries();
        durationText.setText("0");
        sizeText.setText("0");
    }   
}
