package Views;

import javax.swing.*;
import java.awt.*;
import ViewModels.LibroViewModel;
import Models.Libro;
import Models.Gender;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Util.SigbUtilities;



/**
 * Nuevo libro
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class NewLibrosPage extends NewItemPage
{
    /**
     * Contructor
     */
    public NewLibrosPage(Application app)
    {
        super(app);
    }
    
    /**
     * Entries de los campos específicos del libro
     */
    protected JTextField autorText;
    protected JTextField paginasText;
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new LibroViewModel(App);
    }
    
    /**
     * Título de la página
     */
    String getTitlePage()
    {
        return "Nuevo libro";
    }
    
    /**
     * Tamaño de la ventana
     */
    void setSizePage()
    {
        setSize(500, 220);
    }
    
    /**
     * Configurar los datos específicos
     */
    @Override
    protected void configureFieldsData()
    {
        super.configureFieldsData();
     
        JLabel label;
        
        label = new JLabel("Autor: ", JLabel.TRAILING);
        autorText = new JTextField(10);
        label.setLabelFor(autorText);
        panel.add(label);
        panel.add(autorText);
        ++numPairs;
        
        label = new JLabel("Páginas: ", JLabel.TRAILING);
        paginasText = new JTextField(10);
        label.setLabelFor(paginasText);
        panel.add(label);
        panel.add(paginasText);
        ++numPairs;
        
        putButtons();
        
        SpringUtilities.makeCompactGrid(panel,
            numPairs, 2, //rows, cols
            6, 6,        //initX, initY
            6, 6);       //xPad, yPad        
        
    }
    

    /**
     * Validaciones específicas del item
     */
    @Override
    protected boolean validateFields()
    {
        // Primero valido los campos comunes
        boolean result = super.validateFields();
        
        if (result) {
            if (!SigbUtilities.tryParseInt(paginasText.getText())) {
                JOptionPane.showMessageDialog(null, "Debe introducir un número de páginas");
                result = false;
            }
        }
        return result;
    }
    
    /**
     * Borrar las cajas de texto
     */
    @Override
    protected void clearEntries()
    {
        super.clearEntries();
        autorText.setText("");
        paginasText.setText("0");
    }

    /**
     * Preparar el item para almacenarlo
     */
    protected Libro prepareItem()
    {
        // Generar un nuevo item y darle los valores de los Entries
        Libro item = (Libro)viewModel.getType();
        item.setIsbn(isbnText.getText());
        item.setTitle(nameText.getText());
        item.setEditorial(editorialText.getText());
        item.setAutor(autorText.getText());
        item.setGenero((Gender)genderPicker.getSelectedItem());
        item.setPaginas(Integer.parseInt(paginasText.getText()));
        return item;
    }
}
