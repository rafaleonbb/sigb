package Views;

import ViewModels.MultaViewModel;
import Models.Multa;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Vista de las multas
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class MultasPage extends ViewBase
{
    /**
     * Contructor
     */
    public MultasPage(Application app)
    {
        super(app);
    }
    
    /**
     * Instanciar el viewmodel
     */
    void createViewModel()
    {
        viewModel = new MultaViewModel(App);
    }
    
    /**
     * Título de la ventana
     */
    String getTitlePage()
    {
        return "Multas";
    }
    
    
    /**
     * Inicializar componentes de la pantalla
     */
    void initializeComponents()
    {
        configure();

        configureMenu();
        configureList();
    }
    
    /**
     * Configurar el menú de la opción
     */
    protected void configureMenu()
    {
        JMenuBar barra = new JMenuBar();
        setJMenuBar(barra);
        
        JMenu menu = new JMenu("Multas");
        barra.add(menu);
        
        JMenuItem item;
        item = new JMenuItem("Pagar multa");
        item.addActionListener(new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                if (list.getSelectedValue() != null) {
                    Multa multa = (Multa)list.getSelectedValue();
                    ((MultaViewModel)viewModel).cancelarMulta(multa);
                }
            }
        });     
        menu.add(item);
        
        // Los bibliotecarios pueden sacar listados de los préstamos
        if (App.isAdmin()) {
        }
    }
    
}
