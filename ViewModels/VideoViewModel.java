package ViewModels;

import Models.Video;
import Views.Application;


/**
 * Modelo de la vista de los videos
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class VideoViewModel extends BaseViewModel<Video>
{
    /**
     * Constructor
     */
    public VideoViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Video getType()
    {
        return new Video();
    }
}
