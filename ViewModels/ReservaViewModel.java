package ViewModels;

import Models.Item;
import Models.Reserva;
import Models.Prestamo;
import Models.Database;
import Models.IAlmacenable;
import Views.Application;
import java.time.LocalDateTime;
import javax.swing.DefaultListModel;
import java.util.ArrayList;


/**
 * Modelo de vista de las reservas
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class ReservaViewModel extends BaseViewModel<Reserva>
{
    /**
    * Lista para mostrar en el JList
    */
    protected DefaultListModel<Reserva> visibledata;        
    public DefaultListModel<Reserva> getVisibleData() {  return visibledata; }
    
    public ReservaViewModel(Application app)
    {
        super(app);
        isreservavm = true;
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Reserva getType()
    {
        return new Reserva();
    }

    /**
     * Sobrecarga para dejar las reservas del usuario activo si no es un bibliotecario
     */
    protected void loadData()
    {
        super.loadData();
        
        // Copia para mostrar
        visibledata = new DefaultListModel<Reserva>();
        for (Object p : data.toArray()) {
            visibledata.addElement((Reserva)p);
        }
        
        // Dejar sólo los registros del usuario actual
        if (!App.isAdmin()) {
            for (Object p : visibledata.toArray()) {
                Reserva reserva = (Reserva)p;
                if (!reserva.getUsuario().getCodigo().equals(App.getUsuario().getCodigo())) {
                    visibledata.removeElement(reserva);
                }
            }
        }
    }
    
    /**
     * Elimina un elemento de la lista
     */
    public boolean delete(Reserva x)
    {
        boolean result = super.delete(x);
        if (result) {
            getVisibleData().removeElement(x);
        }
        return result;
    }
    
    /**
     * Muestra un aviso si el usuario logado tiene en préstamo algún material reservado
     */
    public void avisoReservasEnPrestamo()
    {
        ArrayList<Item> items = new ArrayList<Item>();
        for (Object o : data.toArray()) {
            Reserva reserva = (Reserva)o;
            
            // Ver si el usuario tiene el elemento en préstamo
            for (Object p : getPrestamos().toArray()) {
                Prestamo prestamo = (Prestamo)p;
                if (prestamo.getFechaDevuelto() == null && 
                    prestamo.getUsuario().equals(App.getUsuario()) && 
                    prestamo.getElemento().equals(reserva.getElemento())
                   ) {
                    items.add(reserva.getElemento());
                }
            }
        }
       
        // Componer y mostrar el aviso
        if (items.size() != 0) {
            String message = "Los siguientes elementos se encuentran en su poder y algún usuario los ha reservado.\nPor favor, devuélvalos lo antes posible:\n";
            for (Item item : items) {
                message += item.getTitle() + "\n";
            }
            App.displayAlert(message);                                
        }
    }
    
    /**
     * Aviso si algun elemento de los que tiene el usuario en reserva ya está disponible
     */
    public void avisoReservasDisponible()
    {
        Database<IAlmacenable> db = new Database<IAlmacenable>();
        ArrayList<Item> items = new ArrayList<Item>();
        for (Object o : data.toArray()) {
            Reserva reserva = (Reserva)o;
            
            if (reserva.getUsuario().equals(App.getUsuario())) {
                // Cargar los datos del tipo de la reserva
                DefaultListModel<IAlmacenable> coleccion = db.loadData(reserva.getElemento());
                
                // Ver si es el mismo que el de la reserva y añadir al List para luego mostrar lo en el aviso
                for (Object i : coleccion.toArray()) {
                    Item item = (Item)i;
                    if (item.getDisponible() && item.equals(reserva.getElemento())) {
                        items.add(item);
                        break;
                    }
                }
            }            
        }
       
        // Componer y mostrar el aviso
        if (items.size() != 0) {
            String message = "Los siguientes elementos que estaban reservados ya se encuentran disponibles:\n";
            for (Item item : items) {
                message += item.getTitle() + "\n";
            }
            App.displayAlert(message);                                
        }
    }
}
