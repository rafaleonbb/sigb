package ViewModels;

import Models.Audio;
import Views.Application;


/**
 * Modelo de la vista de los audios
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class AudioViewModel extends BaseViewModel<Audio>
{
    /**
     * Constructor
     */
    public AudioViewModel(Application app)
    {
        super(app);
    }
    
    /**
     * Implementación del método abstracto de la superclase que devuelve una instancia IAlmacenable 
     */
    public Audio getType()
    {
        return new Audio();
    }
}
