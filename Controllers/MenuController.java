package Controllers;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import Views.*;

/**
 * Controlador del menú
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class MenuController implements ActionListener
{
    public final Application App;
    
    public MenuController(Application app)
    {
        super();
        
        App = app;
    }
    
    public void actionPerformed(ActionEvent e)
    {
        JFrame page = null;
        switch (e.getActionCommand()) {
            case "_LISTA_GENEROS_":
                page = new GendersPage(App);
                break;
                
            case "_NUEVO_GENERO_":
                page = new NewGendersPage(App);
                break;
                
            case "_LISTA_LIBROS_":
                page = new LibrosPage(App);
                break;
                
            case "_NUEVO_LIBRO_":
                page = new NewLibrosPage(App);
                break;
                
            case "_LISTA_REVISTAS_":
                page = new RevistasPage(App);
                break;
                
            case "_NUEVO_REVISTA_":
                page = new NewRevistasPage(App);
                break;
                
            case "_LISTA_PERIODICOS_":
                page = new PeriodicosPage(App);
                break;
                
            case "_NUEVO_PERIODICO_":
                page = new NewPeriodicosPage(App);
                break;
                
            case "_LISTA_AUDIOS_":
                page = new AudiosPage(App);
                break;
                
            case "_NUEVO_AUDIO_":
                page = new NewAudiosPage(App);
                break;
                
            case "_LISTA_VIDEOS_":
                page = new VideosPage(App);
                break;
                
            case "_NUEVO_VIDEO_":
                page = new NewVideosPage(App);
                break;
                
            case "_QUICK_SEARCH_":
                page = new QuickSearchPage(false);
                break;
                
            case "_SEARCH_":
                page = new QuickSearchPage(true);
                break;
                
            case "_LISTA_SUSCRIPCIONES_":
                page = new SuscripcionesPage(App);
                break;
                
            case "_NUEVA_SUSCRIPCION_":
                page = new NewSuscripcionesPage(App);
                break;
                
            case "_LISTA_USUARIOS_":
                page = new UsuariosPage(App);
                break;
                
            case "_NUEVO_USUARIO_":
                page = new NewUsuariosPage(App);
                break;
                
            case "_LISTA_BIBLIOTECARIOS_":
                page = new BibliotecariosPage(App);
                break;
                
            case "_NUEVO_BIBLIOTECARIO_":
                page = new NewBibliotecariosPage(App);
                break;
                
            case "_PRESTAMOS_":
                page = new PrestamosPage(App);
                break;
                
            case "_MULTAS_":
                page = new MultasPage(App);
                break;
                
            case "_RESERVAS_":
                page = new ReservasPage(App);
                break;
                
        } // switch
        
        // Mostrar la ventana
        if (page != null) {
            page.setVisible(true);
        }
        
    } // ActionPerformed
}