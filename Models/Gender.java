package Models;

import java.io.Serializable;

/**
 * Generos de los items
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Gender implements Serializable, IAlmacenable
{
   String name;
   public String getName () { return name; }
   public void setName (String n) { name = n; }
   
   /**
    * Texto que se mostrará en las listas
    */
   public String toString()
   {
       return getName();
   }
}
