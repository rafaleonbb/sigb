package Models;

import javax.swing.DefaultListModel;
import java.io.*;

/**
 * Clase para implementar la lectura y escritura de los datos
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Database<IAlmacenable>
{ 
  
   /**
    * Carga los datos de un fichero de texto
    */
   public DefaultListModel<IAlmacenable> loadData (IAlmacenable type)
   {
       String fileName = type.getClass().getSimpleName() + ".txt";
       DefaultListModel<IAlmacenable> data = new DefaultListModel<IAlmacenable>();
       File file = new File(fileName);
       if (file.exists()) {
           try {
               FileInputStream f = new FileInputStream(fileName);
               ObjectInputStream o = new ObjectInputStream(f);
               data = (DefaultListModel<IAlmacenable>)o.readObject();
               o.close();
               f.close();
           } catch (IOException e) {
               e.printStackTrace();
           } catch (ClassNotFoundException e) {
               e.printStackTrace();
           }
       }       
       return data;
   }
   
   
   /**
    * Serializa los datos y los almacena en un fichero de texto
    */
   public void saveData(IAlmacenable type, DefaultListModel<IAlmacenable> data) 
   {
       String fileName = type.getClass().getSimpleName() + ".txt";
       try {
           FileOutputStream f = new FileOutputStream(fileName);
           ObjectOutputStream o = new ObjectOutputStream(f);
           o.writeObject(data);
           o.close();
           f.close();
       } catch (IOException e) {
           e.printStackTrace();
       }               
   }
   
}
