package Models;

import java.util.Date;
import java.io.Serializable;


/**
 * Suscripciones
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class Suscripcion implements Serializable, IAlmacenable
{
    /**
     * Item suscribible
     */
    ISuscribible item;
    public ISuscribible getItem() { return item; }
    public void setItem(ISuscribible suscribible) { item = suscribible; }
    
    /**
     * Fecha de inicio de la suscripción
     */
    Date fechaInicio;
    public Date getFechaInicio() { return fechaInicio; }
    public void setFechaInicio(Date fecha) { fechaInicio = fecha; }
    
    /**
     * Periodicidad de la suscripción
     */
    Periodicidad periodicidad;
    public Periodicidad getPeriodicidad() { return periodicidad; }
    public void setPeriodicidad(Periodicidad p) { periodicidad = p; }
    
    /**
     * Lo que se mostrará en la lista
     */
    public String toString() { return item.getClass().getSimpleName() + " - " + ((Item)item).getTitle() + " (" + getPeriodicidad().toString() + ")"; }    
}
