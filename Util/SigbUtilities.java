package Util;


/**
 * Clase estática para incluir utilidades
 * 
 * @author (Rafa León) 
 * @version (@2016)
 */
public class SigbUtilities
{
    public static boolean tryParseInt(String value) {  
         try {  
             Integer.parseInt(value);  
             return true;  
          } catch (NumberFormatException e) {  
             return false;  
          }  
    }    
}
